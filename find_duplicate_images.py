import cv2
import time
from itertools import combinations
from multiprocessing import Process, Pool
from os import listdir
from os.path import isfile, join
from sys import argv
from tqdm import tqdm
from are_duplicates import match_by_hist, match_by_size

def matching_pairs(args):
    directory, image_pairs, threshold = args
    duplicate_pairs = []
    image_pair_count = len(image_pairs)
    for i, pair in tqdm(enumerate(image_pairs), total=image_pair_count, unit="pairs"):
        if match_by_hist(cv2.imread(directory+"/"+pair[0]), cv2.imread(directory+"/"+pair[1]), threshold):
        #if match_by_size(directory+"/"+pair[0], directory+"/"+pair[1]):
            duplicate_pairs.append((pair[0], pair[1]))
    return duplicate_pairs

def main():
    if(len(argv) != 3):
        print("usage: " + argv[0] + " directory threshold")
        return -1
    directory = argv[1]
    threshold = float(argv[2])
    images = [f for f in listdir(directory) if isfile(join(directory, f))]
    image_pairs = list(combinations(images, 2))
    image_pair_count = len(image_pairs)
    chunk_count = 15
    image_pair_chunks = [image_pairs[i::chunk_count] for i in range(chunk_count)]
    duplicate_pairs = []
    t = time.time()
    pool = Pool(processes=chunk_count)
    duplicate_pairs = [pair for pairs in pool.map_async(matching_pairs, ((directory, image_pair_chunk, threshold,) for image_pair_chunk in image_pair_chunks)).get() for pair in pairs]
    for pair in duplicate_pairs:
       print(pair[0] + " " + pair[1])
    print("Found " + str(len(duplicate_pairs)) + " duplicates in " + str(time.time() - t) + " seconds.")

if __name__== "__main__":
    main()
