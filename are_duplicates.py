import cv2
from itertools import combinations
from os import listdir
from os.path import getsize, isfile, join
from sys import argv

def blend_frames(file):
    capture = cv2.VideoCapture(file)
    n = 0
    while capture.isOpened():
        ret, frame = capture.read()
        if ret:
            n += 1
        else:
            break        
    capture.release()
    capture = cv2.VideoCapture(file)
    if capture.isOpened():
        ret, frame = capture.read()
        if ret:
            res = frame
        else:
            return None
    i = 1
    while capture.isOpened():
        ret, frame = capture.read()
        if ret:
            alpha = 1.0/(i + 1)
            beta = 1.0 - alpha
            res = cv2.addWeighted(frame, alpha, res, beta, 0.0)
            i += 1
        else:
            break
    return res

def compare_with_sift(sift, flann, image1, image2, image1_mask=None, image2_mask=None):
    kp_1, desc_1 = sift.detectAndCompute(image1, image1_mask)
    kp_2, desc_2 = sift.detectAndCompute(image2, image2_mask)
    return -1 if len(kp_1) < 2 or len(kp_2) < 2 else len([m for m, n in flann.knnMatch(desc_1, desc_2, k=2) if m.distance < 0.6 * n.distance]) / (len(kp_1) if len(kp_1) >= len(kp_2) else len(kp_2)) * 100

def match_by_hist(image1, image2, percentage):
    image1 = cv2.cvtColor(image1, cv2.COLOR_BGR2RGB)
    image2 = cv2.cvtColor(image2, cv2.COLOR_BGR2RGB)
    hist1 = cv2.calcHist([image1], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    hist2 = cv2.calcHist([image2], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
    hist1 = cv2.normalize(hist1, hist1).flatten()
    hist2 = cv2.normalize(hist2, hist2).flatten()
    return cv2.compareHist(hist1, hist2, cv2.HISTCMP_CORREL) > percentage / 100.0 

def match_by_sift(image1, image2, percentage):
    sift = cv2.xfeatures2d.SIFT_create()
    flann = cv2.FlannBasedMatcher(dict(algorithm=0, trees=5), dict())
    return compare_similarity(sift, flann, image1, image2) > percentage / 100.0

def match_by_size(file1, file2):
    return getsize(file1) == getsize(file2)

def main():
    if(len(argv) != 4):
        print("usage: " + argv[0] + " file1 file2 threshold")
        return -1
    file1 = argv[1]
    file2 = argv[2]
    threshold = float(argv[3])
    print(match_by_hist(cv2.imread(file1), cv2.imread(file2), threshold))

if __name__== "__main__":
    main()

