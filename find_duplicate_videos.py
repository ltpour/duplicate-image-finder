import cv2
import os
import time
from itertools import combinations
from multiprocessing import Process, Pool
from os import listdir
from os.path import isfile, join
from sys import argv
from tqdm import tqdm
from are_duplicates import match_by_hist, match_by_size, blend_frames

def matching_pairs(args):
    video_pairs, blended_frames, threshold = args
    duplicate_pairs = []
    video_pair_count = len(video_pairs)
    for i, pair in tqdm(enumerate(video_pairs), total=video_pair_count, unit="pairs"):
        if match_by_hist(blended_frames[pair[0]], blended_frames[pair[1]], threshold):
            duplicate_pairs.append((pair[0], pair[1]))
    return duplicate_pairs

def blend_frames_for_videos(args):
    directory, videos = args
    blended_frames = {}
    os.makedirs("output", exist_ok=True)
    for video in videos:
        blended_frames[video] = blend_frames(directory+"/"+video)
        cv2.imwrite("output/" + video[:-4] + ".png", blended_frames[video])
    return blended_frames

def main():
    chunk_count = 15
    if(len(argv) != 3):
        print("usage: " + argv[0] + " directory threshold")
        return -1
    directory = argv[1]
    threshold = float(argv[2])
    t = time.time()    
    videos = [f for f in listdir(directory) if isfile(join(directory, f))]
    
    video_chunks = [videos[i::chunk_count] for i in range(chunk_count)]
    pool = Pool(processes=chunk_count)
    ds = pool.map_async(blend_frames_for_videos, ((directory, video_chunk,) for video_chunk in video_chunks)).get()
    blended_frames = {k: v for d in ds for k, v in d.items()}

    video_pairs = list(combinations(videos, 2))
    video_pair_chunks = [video_pairs[i::chunk_count] for i in range(chunk_count)]
    duplicate_pairs = []
    pool = Pool(processes=chunk_count)
    duplicate_pairs = [pair for pairs in pool.map_async(matching_pairs, ((video_pair_chunk, blended_frames, threshold,) for video_pair_chunk in video_pair_chunks)).get() for pair in pairs]
    for pair in duplicate_pairs:
       print(pair[0] + " " + pair[1])
    print("Found " + str(len(duplicate_pairs)) + " duplicates in " + str(time.time() - t) + " seconds.")

if __name__== "__main__":
    main()
